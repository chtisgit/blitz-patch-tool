Strict
Import brl.filesystem
Import brl.system 

Type File
	Field path$
	Field data:Byte[]
	Field is_blitzplus = 0
	Field is_blitz3d = 0
	Field has_dplay = 1
	Field saved = 0
	Field data_changed = 0

	Function backuppath$(path$)
		Return path+".backup"
	EndFunction
	
	Method restore()
		If FileType(backuppath(path)) <> FILETYPE_FILE Then Return False
		
		DeleteFile path
		RenameFile backuppath(path), path
		init
		
		Return True
	EndMethod
	
	Method save(altpath$ = Null)
		If Not data_changed Then Return
		
		If saved = 0 Then
			saved = 1
			CopyFile path, backuppath(path)
		EndIf
		
		SaveByteArray data, path
		' SaveByteArray throws sometimes randomly
		' if it didn't, set data_changed to false
		data_changed = 0
	EndMethod
	
	Method init()
		data = LoadByteArray(path)
		is_blitzplus = 0
		is_blitz3d = 0
		has_dplay = 1
		saved = 0
		data_changed = 0
					
		If findinfile("%CreateWindow$text%x%y%width%height%group=0%style=15") >= 0 Then
			is_blitzplus = 1
		EndIf
		
		If findinfile("AnimateMD2%md2%mode=1#speed=1%first_frame=0%last_frame=9999") >= 0 Then
			is_blitz3d = 1
		EndIf
		
		If findinfile("DPLAYX.DLL") = -1 And findinfile("dplayx.dll") = -1 Then
			has_dplay = 0
		EndIf
	
	EndMethod
	
	Function Load:File(path$)
		Local f:File = New File
		f.path = path
			
		If Len(path) = 0 Then Return Null
		
		f.init
		Return f
	EndFunction
	
	Method close()
		data = Null
		path = Null
	EndMethod
	
	Method removeDPlay()
		Assert (is_blitz3d Or is_blitzplus) And Not (is_blitz3d And is_blitzplus) And has_dplay
		
		Local success
		
		If is_blitz3d Then
			success = replacestring("DPLAYX.DLL","MSVCRT.DLL~0")
		Else If is_blitzplus Then
			success = replacestring("dplayx.dll","MSVCRT.DLL~0")
			success = success And replacestring("DirectPlayLobbyCreateA","printf~0")
		EndIf
		
		Return success
	EndMethod
		
	Method replacestring%(str1$,str2$)
		Local p = findinfile(str1)
		If p >= 0 Then
			patch p, str2
			Return True
		EndIf
		Return False
	EndMethod
	
	Method patch(pos, str$)
		data_changed = True
		For Local i  = 0 Until Len(str)
			data[pos+i] = str[i]
		Next
	EndMethod
	
	Method findinfile(str$)
		Local PT:Int[Len(str)+1], i = 0, j = -1
	
		PT[i] = j
		While i < Len(str)
			While j >= 0 And str[j] <> str[i]
				j = PT[j]
			Wend
			i :+ 1
			j :+ 1
			PT[i] = j
		Wend
		
		i = 0
		j = 0
		
		While i < Len(data)
			While j >= 0 And data[i] <> str[j]
				j = PT[j]
			Wend
			
			i :+ 1
			j :+ 1
			
			If j = Len(str) Then
				Return i - Len(str)
			EndIf
		
		Wend
	
		Return -1
	EndMethod


EndType

