Strict
Framework brl.blitz
Import brl.filesystem
Import brl.eventqueue
Import maxgui.drivers

Import "btool-internal.bmx"

Global workfile:File = Null

Local win:TGadget = CreateWindow("Blitz3D/BlitzPlus Patch Tool",0,0,400,200,Null,WINDOW_TITLEBAR|WINDOW_CENTER|WINDOW_HIDDEN|WINDOW_ACCEPTFILES|WINDOW_MENU)
Local filemenu:TGadget = CreateMenu("File",0,win)
Global  rm_dplay:tgadget = CreateButton("Remove DirectPlay",10,110,100,36,win)
Global status_area:TGadget = CreateTextArea(0,0,ClientWidth(win),100,win,TEXTAREA_READONLY|TEXTAREA_WORDWRAP)
CreateMenu "Open",1,filemenu,KEY_O,MODIFIER_COMMAND
CreateMenu "Save",3,filemenu,KEY_S,MODIFIER_COMMAND
CreateMenu "Restore backup",4,filemenu,KEY_Z,MODIFIER_COMMAND
CreateMenu "Exit",2,filemenu,KEY_F4,MODIFIER_ALT
UpdateWindowMenu win
initstatus

ShowGadget win

Repeat

	WaitEvent

	Select EventID()
	Case EVENT_WINDOWCLOSE
		Exit
	Case EVENT_GADGETACTION
		If EventSource() = rm_dplay Then removedplay()
	Case EVENT_MENUACTION
		Select EventData()
		Case 1
			loade RequestFile("Blitz executable", "exe")
		Case 2
			Exit
		Case 3
			save
		Case 4
			restore
		EndSelect
	Case EVENT_WINDOWACCEPT
		Local p$ = String(EventExtra())
		If p.endswith(".exe") Then
			loade p
		EndIf
	EndSelect

Forever

End

Function save()
	If workfile Then
		Local really_saved = False, dont_save = False
		Repeat
			Try
				workfile.save
				really_saved = True
			Catch e:TStreamWriteException
				dont_save = Not Confirm("Could not save file. Try again?")
			EndTry
		Until really_saved Or dont_save
		If really_saved Then
			AddTextAreaText status_area, "Saved changes to file.~n"
		Else
			AddTextAreaText status_area, "File was not saved!~n"
		EndIf
	EndIf
EndFunction

Function initstatus()
	DisableGadget rm_dplay

	If workfile = Null Then
		AddTextAreaText status_area, "~nLoaded: <none>~n"
		Return	
	EndIf
		
	AddTextAreaText status_area, "~nLoaded: "+workfile.path+"~n"
	
	If workfile.is_blitzplus Then
		AddTextAreaText status_area, "BlitzPlus executable detected~n"
	EndIf
	
	If workfile.is_blitz3d Then
		AddTextAreaText status_area, "Blitz3D executable detected~n"
	EndIf
	
	If workfile.has_dplay Then
		AddTextAreaText status_area, "DirectPlay dependency found!~n"
	Else
		AddTextAreaText status_area, "NO DirectPlay dependency found!~n"
	EndIf
	
	If workfile.is_blitz3d Or workfile.is_blitzplus Then
		If workfile.has_dplay Then
			EnableGadget rm_dplay
		Else
			AddTextAreaText status_area, "This executable is already patched~n"
		EndIf
	EndIf
EndFunction

Function restore()
	If Not workfile Then Return
	AddTextAreaText status_area, "Restoring from backup...~n"
	workfile.restore
	initstatus
EndFunction

Function loade:File(path$)
	workfile = File.Load(path)
	initstatus
EndFunction

Function removeDPlay()	
	If workfile.removeDPlay() Then
		AddTextAreaText status_area, "DirectPlay was removed successfully!~n"
	Else
		AddTextAreaText status_area, "Error while patching executable!~n"
	EndIf
	
	DisableGadget rm_dplay
EndFunction


