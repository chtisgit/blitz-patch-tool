/*
 * Blitz Patch Tool
 * by Thunder (blitzforum.de)
 *
 * good tool to patch Blitz3D and BlitzPlus executables so
 * they don't load DirectPlay
 *
 *
 * License: Public Domain
 *
 */

package main

import "os"
import "regexp"
import "io"
import "io/ioutil"
import "fmt"

const DUMMY_DLL = "msvcrt.dll"
const DUMMY_FUNCTION = "printf"
const BACKUP_EXT = ".backup"

var blitzplus_sig *regexp.Regexp = regexp.MustCompile("%CreateWindow$text%x%y%width%height%group=0%style=15")
var blitz3d_sig *regexp.Regexp = regexp.MustCompile("AnimateMD2%md2%mode=1#speed=1%first_frame=0%last_frame=9999")
var dplay_sig *regexp.Regexp = regexp.MustCompile("[Dd][Pp][Ll][Aa][Yy][Xx]\\.[Dd][Ll][Ll]\x00")
var lobby_sig *regexp.Regexp = regexp.MustCompile("DirectPlayLobbyCreateA\x00")
var msvcrt_sig *regexp.Regexp = regexp.MustCompile(DUMMY_DLL)

func copyfile(src, dst string) error {
	in, err := os.Open(src)
	if err != nil {
		return err
	}
	defer in.Close()

	out, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, in)
	if err != nil {
		return err
	}
	return out.Close();
}
func backup(name string) error {
	return copyfile(name,name+BACKUP_EXT)
}
func restore(name string) error {
	return copyfile(name+BACKUP_EXT,name)
}

func overwrite(b []byte, m []byte, offset int) {
	for i := 0; i < len(m); i++ {
		b[offset+i] = m[i]
	}
}

func patchFile(name string, st *os.FileInfo) bool {
	fmt.Print("Reading file ",name,"... ")
	data, err := ioutil.ReadFile(name)

	if err != nil {
		fmt.Println("FAIL")
		return false
	}
	is_bplus,is_b3d,has_dplay := false,false,false

	if blitzplus_sig.FindIndex(data) != nil {
		is_bplus = true
	}
	if blitz3d_sig.FindIndex(data) != nil {
		is_b3d = true
	}
	I := dplay_sig.FindIndex(data)
	if I != nil {
		has_dplay = true
	}

	if is_bplus && is_b3d {
		fmt.Println("AMBIGUOUS")
		return false
	}
	if !is_bplus && !is_b3d {
		fmt.Println("NOT BB")
		return false
	}
	if !has_dplay{
		fmt.Println("HAS NO DPLAY")
		return false
	}
	backup(name)

	// patching...
	overwrite(data, []byte(DUMMY_DLL), I[0])
	if is_bplus {
		J := lobby_sig.FindIndex(data)
		if J == nil {
			fmt.Println("FAILED")
			return false
		}
		overwrite(data, append([]byte(DUMMY_FUNCTION),'\x00'), J[0])
	}

	// overwrite the file
	file,err := os.Create(name)
	defer file.Close()

	if err != nil {
		fmt.Println("FAILED")
		return false
	}
	if _,err := file.Write(data); err != nil {
		fmt.Println("FATAL")
		restore(name)
		fmt.Println("restored "+name+" from backup.")
		return false
	}

	fmt.Println("OK")
	return true
}

func main(){
	if len(os.Args) <= 1 {
		fmt.Println("usage: bptool.exe <exe file> [<exe file> ...]")
		return
	}
	successes := 0
	for i := 1; i < len(os.Args); i++ {
		if st,err := os.Stat(os.Args[i]); err == nil {
			if patchFile(os.Args[i],&st) {
				successes++
			}
		}else{
			fmt.Println("cannot stat ",os.Args[i],err)
		}
	}
	fmt.Println(successes,"/",len(os.Args)-1," succeeded!")
}
